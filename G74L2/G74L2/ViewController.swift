//
//  ViewController.swift
//  G74L2
//
//  Created by Ivan Vasilevich on 9/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		var ivansAge: Int = 26
		print("Vanya is \(ivansAge) years old")
		let pi = 3.14159
		print("pi =", pi)
		ivansAge = 27
		print("Vanya is \(ivansAge) years old")
		
		//		var mishansAge: Int
		
		//		+ - * / %
		let sumOfAgeAndPi = ivansAge + Int(pi)
		print("sum Of Age And Pi", sumOfAgeAndPi)
		//		16 % 5 == 1
		
		for _ in 0..<100 {
			ivansAge = ivansAge + 1
			//			+= -= *= /= %=
			ivansAge += 1
			print("Vanya is \(ivansAge) years old")
		}
		
		print("Vanya is \(ivansAge) years old")
		
		
		
		getRandomStudent()
		
		forLoop()
		
	}
	
	func getRandomStudent() {
		var ivansAge = 33
		let randomNumber = Int.random(in: 0..<10)
		
		//		print("throw free candy to student #\(randomNumber)")
		
		//		< > <= >= == !=
		if randomNumber == 0 {
			let luckyStudent = randomNumber
			print("ne dawat6 student #\(luckyStudent)")
		}
		let throwCandyLeft = randomNumber > 6
		if throwCandyLeft {
			print("THROW FREE CANDY TO STUDENT #\(randomNumber)")
		}
		
		//		- +
		print("ivansAge + randomNumber =", -ivansAge + +randomNumber)
		
		//		let a = 5, b = 1
		//		print(+b)
		
		//		!Bool
		if !throwCandyLeft {
			
		}
		
		let isYoung = ivansAge < 50
		//		or || / and && / XOR ^
		if isYoung || throwCandyLeft {
			print("Life is good")
		}
		else {
			print("WASYED")
		}
	}
	
	func forLoop() {
		for i in 0..<5 {
			print(i)
		}
	}
	
	
}

