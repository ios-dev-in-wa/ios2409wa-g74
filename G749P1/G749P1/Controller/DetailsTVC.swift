//
//  DetailsTVC.swift
//  G749P1
//
//  Created by Ivan Vasilevich on 22.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DetailsTVC: UITableViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	
	var fruitToDisplay: Fruit!
	
	// MARK: - ViewControllerLifecycle
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		imageView.image = UIImage(named: fruitToDisplay.name)
		nameLabel.text = fruitToDisplay.name
		
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
		return super.tableView(tableView, numberOfRowsInSection: section)
    }

}
