//
//  ViewController.swift
//  G749P1
//
//  Created by Ivan Vasilevich on 22.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
    
	var items = [Fruit]() {
		didSet {
			updateCountLabel()
			tableView.reloadData()
		}
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        let nib = UINib(nibName: FruitTVCell.rIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: FruitTVCell.rIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
		updateCountLabel()
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: "clear", style: .plain, target: self, action: #selector(clearItems))
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "add", style: .plain, target: self, action: #selector(addFruit))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailTVC = segue.destination as? DetailsTVC,
            let sender = sender as? UITableViewCell,
            let index = tableView.indexPath(for: sender)?.row
        {
            let itemToDisplay = items[index]
            detailTVC.fruitToDisplay = itemToDisplay
        }
    }
	
	func updateCountLabel() {
		let countLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.width))
		countLabel.textAlignment = .center
		countLabel.text = "\(items.count) total items count"
		tableView.tableFooterView = countLabel
	}
	
	private func setupData() {
		items = ["Coconut",
				 "Orange",
				 "Persimmon",
				 "Banana",
				 "Apple",
				 "Feijoa",
				 "Grapefruit"]
			.map { (str) -> Fruit in
            return Fruit(name: str)
		}
	}
    
	@objc func clearItems() {
		items.removeAll()
	}
	
	@objc func addFruit() {
		items.insert(Fruit.random(), at: 0)
	}
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: FruitTVCell.rIdentifier, for: indexPath) as! FruitTVCell
		let itemToDisplay = items[indexPath.row]
		cell.nameLabel.text = itemToDisplay.name
		cell.positionLabel.text = (indexPath.row + 1).description
		cell.detailTextLabel?.text = itemToDisplay.name.count.description
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "showDetail", sender: tableView.cellForRow(at: indexPath))
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return indexPath.row == 0 ? 100 : tableView.rowHeight
	}
	
}

