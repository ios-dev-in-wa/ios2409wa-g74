//
//  FruitTVCell.swift
//  G749P1
//
//  Created by Ivan Vasilevich on 22.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class FruitTVCell: UITableViewCell {

	@IBOutlet weak var positionLabel: UILabel!
	@IBOutlet weak var nameLabel: UILabel!
	
	static var rIdentifier: String {
		return "FruitTVCell"
	}
    
}
