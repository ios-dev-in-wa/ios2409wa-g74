//
//  Fruit.swift
//  G74L4
//
//  Created by Ivan Vasilevich on 10/3/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class Fruit: NSObject {
    
    static let shared = Fruit(name: "Shared")
    
    static func random() -> Fruit {
        let randomName = """
            Grapes
            Guava
            Honeydew Melon
            Jackfruit
            Java-Plum
            Jujube Fruit
            Kiwifruit
            Kumquat
            Lemon
            Lime
            Longan
            Loquat
            Lychee
            Mandarin
            Mango
            Mangosteen
            Mulberries
            Nectarine
            Olives
            Orange
            Papaya
            Passion Fruit
            Peaches
            Pear
            Persimmon – Japanese
            Pitaya (Dragonfruit)
            Pineapple
            Pitanga
            Plantain
            Plums
            Pomegranate
            Prickly Pear
            Prunes
            Pummelo
            Quince
            Raspberries
            Rhubarb
            Rose-Apple
            Sapodilla
            """.components(separatedBy: "\n").randomElement()!
        return Fruit(name: randomName)
    }
    
    override var description: String {
        let pesticidesMarker = self.pesticid ? "X" : "I"
        return "(\(pesticidesMarker)) \(name) - (\(weight))g"
    }
    
    let name: String
    let pesticid: Bool
    var weight: Int = 0
    
    init(name: String, pesticid: Bool = Bool.random()) {
        self.name =	name
        self.pesticid = pesticid
    }
}

import Parse

class PFFruit: PFObject, PFSubclassing, Namable {
	
	@NSManaged var name: String?
//	@NSManaged var type: String?
	@NSManaged var owner: PFUser?
//	@NSManaged var pictureFile: PFFileObject?
	
	static func parseClassName() -> String {
		return "PFFruit"
	}
	

}
