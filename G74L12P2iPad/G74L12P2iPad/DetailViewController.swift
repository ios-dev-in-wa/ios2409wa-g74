//
//  DetailViewController.swift
//  G74L12P2iPad
//
//  Created by Ivan Vasilevich on 31.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import AVFoundation

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!
	
	var player: AVAudioPlayer!


	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail.description
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		configureView()
		
		let link = Bundle.main.url(forResource: "pew-pew-lei", withExtension: "caf")!
		player = try! AVAudioPlayer(contentsOf: link)
		player.play()
		
	}

	var detailItem: NSDate? {
		didSet {
		    // Update the view.
		    configureView()
		}
	}


}

