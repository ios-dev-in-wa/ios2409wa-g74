//
//  BlueViewController.swift
//  G74L7
//
//  Created by Ivan Vasilevich on 15.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class BlueViewController: UIViewController {

	@IBOutlet weak var myLabel2: UILabel!
	
	var info = ""
	var prevVC: ViewController!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		view.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 1, alpha: 1)
		myLabel2.text = info
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
