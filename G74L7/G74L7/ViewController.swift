//
//  ViewController.swift
//  G74L7
//
//  Created by Ivan Vasilevich on 15.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var myLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		myLabel.text = "09876543"
		view.backgroundColor = .black
	}

	@IBAction func foo() {
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		print(segue.identifier ?? "no identifier")
		if let dest = segue.destination as? BlueViewController {
			dest.info = "AWESOME4000"
			dest.prevVC = self
		}
	}
	
}

