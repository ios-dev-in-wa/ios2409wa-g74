//
//  ViewController.swift
//  G74L15
//
//  Created by Ivan Vasilevich on 12.11.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

extension Localizable {
	static let doneButtonTitle = NSLocalizedString("Done", comment: "left bar button item title")
	static let viewControllerTitle = NSLocalizedString("Sossage", comment: "")
}

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		setupView()
	}
	
	private func setupView() {
		navigationItem.title = Localizable.viewControllerTitle
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: Localizable.doneButtonTitle, style: .done, target: nil, action: nil)
		
	}


}

