//
//  ViewController.swift
//  G74L8
//
//  Created by Ivan Vasilevich on 17.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate {

	var box: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		addBox()
	}
	
	func addBox() {
		box = FaceView(frame: CGRect(x: 40, y: 40, width: 64, height: 64))
		box.backgroundColor = .red
		(box as? FaceView)?.changeHappyFace(withHappLevel: 35, andColor: .orange)
		view.addSubview(box)
		view.sendSubviewToBack(box)
	}

	@IBAction func pinchRecognized(_ sender: UIPinchGestureRecognizer) {
		box.frame.size.height *= sender.scale
		box.frame.size.width *= sender.scale
		sender.scale = 1
		sender.view!.backgroundColor = UIColor.blue.withAlphaComponent(sender.scale)
		box.tag += 1
		print("foo", box.tag)
		if sender.state == .ended {
			box.setNeedsDisplay()
		}
	}
	
}

