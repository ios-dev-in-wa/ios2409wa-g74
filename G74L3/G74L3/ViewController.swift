//
//  ViewController.swift
//  G74L3
//
//  Created by Ivan Vasilevich on 10/1/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.

//		workWithString()
//		optionalStringNumbers()
//		optionalDefaultValue()
		arrayMadness()
		dixtionary()
	}
	
	func foo() {
		let a = 4.0
//		a.squareRoot()
		squareOfNumber()
//		let sqOf3 = squareOf(number: a) //+ squareOf(number: 5)
		let sumOfSq5and3 = resultSquareOf(number: 5) + resultSquareOf(number: a)
		print("sumOfSq5and3 =", sumOfSq5and3)
		printCoupleOfNumbers(a: 99, b: 33)
		move()
	}

	func squareOfNumber() {
		let number = 5
		let result = number * number
//		return;
		print("square of \(number) is \(result)")
	}
	
	func squareOf(number: Double) {
		let result = number * number
		print("square of \(number) is \(result)")
	}
	
	func resultSquareOf(number: Double) -> Double {
		let result = number * number
		return result
	}
	
	func printCoupleOfNumbers(a: Int, b: Int) {
		print("a = \(a), b = \(b)")
//		print(3.14 + Double(a))
		move(2)
	}
	
	func move(_ steps: Int = 1) {
//		var steps = steps
//		steps += 1
		for i in 0..<steps {
			print("move #\(i + 1)")
		}
		test(with: 5)
	}
	
	func test(with number: Int) {
		print("test with \(number)")
	}
	
	func workWithString() {
		let name = "JoIvan"
//		name.uppercased()
		let pref = "Jo"
		if name.hasPrefix(pref) {
			print("your name starts with \"\(pref)\"")
		}
		else {
			print("your name NOT starts with \"\(pref)\"")
		}
	}
	
	func optionalStringNumbers() {
		let numberString = "5"
		let number = Int(numberString)
		
		//Optional binding
		if let realNumber = number {
			let sum = realNumber + 12
			print(sum)
		}
		else {
			print("enter correct number")
		}
		
		//Force unwrap
		let sum = number! + 12
//
		print(sum)
	}
	
	func optionalDefaultValue() {
		var students: Int?// = 0
		print(students ?? "nil in students")
		students = (students ?? 0) + 1
		print(students ?? "nil in students")
		students = (students ?? 0) + 1
		print(students ?? "nil in students")
	}

	func arrayMadness() {
		let number: Int = 4
		var array: [Int] = [number, 99, 6]
		print("\"\(array)\" consist of \(array.count) elements")
		print(array.last!)
		print(array[1])
		array[1] = 344
		print(array[1])
		let a = array.remove(at: 1)
		print(array[1])
		array.insert(a, at: 0)
		print("\"\(array)\" consist of \(array.count) elements")
		
		for i in 0..<array.count {
			print("array[\(i)] = \(array[i])")
		}
	}
	
	func dixtionary() {
		var phoneBook = [
			"Police" : "102",
			"Ambulance" : "103"
		]
		print(phoneBook["Poli1ce"] ?? "no record found")
		phoneBook["Ivan"] = "34567890"
	}
	
}

