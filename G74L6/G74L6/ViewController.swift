//
//  ViewController.swift
//  G74L6
//
//  Created by Ivan Vasilevich on 10.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		let now = Date.init(timeIntervalSince1970: 1470724159)
		let formatter = DateFormatter(with: "E dd MMM")
		
		let result = formatter.string(from: now)
		print(result)
		
		let runNumber = UserDefaults.standard.integer(forKey: Const.kRunCount) + 1
		print("runNumber: ", runNumber)
		UserDefaults.standard.set(runNumber , forKey: Const.kRunCount)
		
		var a = 3
		let b = a
		a = 4
		print(b)
		print(a == b)
		
		let viewA = UIView()
		viewA.tag = 5
		let viewB = viewA
		viewA.tag = 6
		print(viewB.tag)
		print(viewA == viewB)
		
		let point = CGPoint(x: 15, y: 32)
//		point.x = 54
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let img = UIImage(named: "car")
		imageView.image = img
	}

}

extension DateFormatter {
	convenience init(with format: String) {
		self.init()
		self.dateFormat = format
	}
}
