//
//  ViewController.swift
//  G74L11
//
//  Created by Ivan Vasilevich on 29.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//  https://docs.parseplatform.org/ios/guide/#subclasses

import UIKit
import Parse

class ViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
    
	var items = [Namable]() {
		didSet {
			updateCountLabel()
			tableView.reloadData()
		}
	}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        tableView.dataSource = self
        tableView.delegate = self
		updateCountLabel()
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: "clear", style: .plain, target: self, action: #selector(clearItems))
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: "add", style: .plain, target: self, action: #selector(addFruit))
		setupUser()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
    }
	
	func updateCountLabel() {
		let countLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.width))
		countLabel.textAlignment = .center
		countLabel.text = "\(items.count) total items count"
		tableView.tableFooterView = countLabel
	}
	
	private func setupData() {
		let query = PFFruit.query()
//		query?.findObjectsInBackground(block: { (optionalObjecs, error) in
//			if let realFruits = optionalObjecs as? [PFFruit] {
//				self.items = realFruits
//			} else {
//				print(error!)
//			}
//		})
		NetworkManager.shared.getFruits { (optionalObjecs, error) in
			if let realFruits = optionalObjecs as? [Namable] {
				self.items = realFruits
			} else {
				print(error!)
			}
		}
	}
	
	private func setupUser() {
		if PFUser.current() != nil {
			return
		} else {
			let user = PFUser()

			let alert = UIAlertController(title: "SingIn", message: "provide your credentioals", preferredStyle: .alert)
			alert.addTextField { (textField) in
				textField.placeholder = "Enter User Name"
			}
			alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
				user.username = alert.textFields!.first!.text
				user.password = user.username
				user.signUpInBackground {
					(succeeded, error) -> Void in
					if let error = error {
						// Show the errorString somewhere and let the user try again.
						print(error)
					} else {
						// Hooray! Let them use the app now.
						print("success: \(user.username!)")
					}
				}
			}))
			alert.addAction(UIAlertAction(title: "Canlel", style: .cancel, handler: nil))
			present(alert, animated: true, completion: nil)	
		}
	}
    
	@objc func clearItems() {
		items.removeAll()
	}
	
	@objc func addFruit() {
		setupUser()
		if PFUser.current() != nil {
			let randomFruit = Fruit.random()
			
			let objectToSave = PFFruit()
			objectToSave.owner = PFUser.current()!
			objectToSave.name = randomFruit.name
			items.insert(objectToSave, at: 0)
			objectToSave.saveEventually()
//			PFUser.logOut()
		}
	}
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		let itemToDisplay = items[indexPath.row]
		cell.textLabel?.text = itemToDisplay.name
		cell.detailTextLabel?.text = (itemToDisplay.name?.count ?? 0).description
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "showDetail", sender: tableView.cellForRow(at: indexPath))
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return indexPath.row == 0 ? 100 : tableView.rowHeight
	}
	
}
