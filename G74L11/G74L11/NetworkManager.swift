//
//  NetworkManager.swift
//  G74L11
//
//  Created by Ivan Vasilevich on 14.11.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import Foundation

let kBgQ = DispatchQueue.global(qos: .background)
let kMainQueue = DispatchQueue.main

protocol Namable {
	var name: String? { get }
}

typealias ClosureWithObjectsFromNetwork = ([Namable]?, Error?) -> Void

class NetworkManager {
	
	static let shared = NetworkManager()
	
	func getFruits(completion: @escaping ClosureWithObjectsFromNetwork) {
		/* Configure session, choose between:
		* defaultSessionConfiguration
		* ephemeralSessionConfiguration
		* backgroundSessionConfigurationWithIdentifier:
		And set session-wide properties, such as: HTTPAdditionalHeaders,
		HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
		*/
		let sessionConfig = URLSessionConfiguration.default
		
		/* Create session, and optionally set a URLSessionDelegate. */
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
		
		/* Create the Request:
		Drinks (GET https://parseapi.back4app.com/classes/PFFruit)
		*/
		
		guard var URL = URL(string: "https://parseapi.back4app.com/classes/PFFruit") else {return}
		var request = URLRequest(url: URL)
		request.httpMethod = "GET"
		
		// Headers
		
		request.addValue("5YZpX4evMtao7eiYazavxaGPHhpMLBouN121HVsy", forHTTPHeaderField: "X-Parse-REST-API-Key")
		request.addValue("xfY77b09IbPQOgFTGAPqSS684uJDsHfPtp2i5S27", forHTTPHeaderField: "X-Parse-Application-Id")
		
		/* Start a new Task */
		let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
			if (error == nil) {
				// Success
				let statusCode = (response as! HTTPURLResponse).statusCode
				
				let jsonData = data!
				let fruitsResults = try! JSONDecoder().decode(FruitResults.self, from: jsonData)
				print(fruitsResults.results.first?.name ?? "no fruit name found")
				print("URL Session Task Succeeded: HTTP \(statusCode)")
				kMainQueue.async {
					completion(fruitsResults.results, error)
				}
				
			}
			else {
				// Failure
				completion(nil, error)
				print("URL Session Task Failed: %@", error!.localizedDescription)
			}
		})
		task.resume()
		session.finishTasksAndInvalidate()
	}
	
	func sendRequest() {
		   /* Configure session, choose between:
			  * defaultSessionConfiguration
			  * ephemeralSessionConfiguration
			  * backgroundSessionConfigurationWithIdentifier:
			And set session-wide properties, such as: HTTPAdditionalHeaders,
			HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
			*/
		   let sessionConfig = URLSessionConfiguration.default

		   /* Create session, and optionally set a URLSessionDelegate. */
		   let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

		   /* Create the Request:
			  CreateDrink (POST https://parseapi.back4app.com/classes/PFFruit)
			*/

		   guard var URL = URL(string: "https://parseapi.back4app.com/classes/PFFruit") else {return}
		   var request = URLRequest(url: URL)
		   request.httpMethod = "POST"

		   // Headers

		   request.addValue("5YZpX4evMtao7eiYazavxaGPHhpMLBouN121HVsy", forHTTPHeaderField: "X-Parse-REST-API-Key")
		   request.addValue("xfY77b09IbPQOgFTGAPqSS684uJDsHfPtp2i5S27", forHTTPHeaderField: "X-Parse-Application-Id")
		   request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

		   // JSON Body

		   let bodyObject: [String : Any] = [
			   "name": "Orange"
		   ]
		   request.httpBody = try! JSONSerialization.data(withJSONObject: bodyObject, options: [])

		   /* Start a new Task */
		   let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
			   if (error == nil) {
				   // Success
				   let statusCode = (response as! HTTPURLResponse).statusCode
				   print("URL Session Task Succeeded: HTTP \(statusCode)")
			   }
			   else {
				   // Failure
				   print("URL Session Task Failed: %@", error!.localizedDescription);
			   }
		   })
		   task.resume()
		   session.finishTasksAndInvalidate()
	   }
}


struct FruitResults: Codable {
	let results: [FruitContainer]
}

struct FruitContainer: Codable, Namable {
	let name: String?
}
