//
//  AppDelegate.swift
//  G74L11
//
//  Created by Ivan Vasilevich on 29.10.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		IQKeyboardManager.shared.enable = true
		initParse()
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}
	
	private func initParse() {
		
		let configuration = ParseClientConfiguration { (config) in
			config.applicationId = "xfY77b09IbPQOgFTGAPqSS684uJDsHfPtp2i5S27"
			config.clientKey = "05L7PmdipbQwTZVrtoKw0wKw9DWWb18ir8vPaZok"
			config.server = "https://parseapi.back4app.com"
		}
		Parse.initialize(with: configuration)
		
		PFConfig.getInBackground()
		
	}

}
