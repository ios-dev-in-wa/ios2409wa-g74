//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
/*

*/

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
	
	func l0c() {
		move()
		if frontIsClear {
			doubleMove()
			turnRight()
			pick()
			move()
			turnLeft()
			doubleMove()
			put()
			doubleMove()
		}
		else {
			turnLeft()
		}
	}
	
	func forLoopExample() {
		for _ in 0..<15 {
			put()
			move()
		}
	}
	
	func whileLoopExample() {
		while noCandyPresent {
			if frontIsBlocked {
				break
			}
			move()
		}
		
		turnLeft()
	}
	
	//  Level name setup
	override func viewDidLoad() {
		levelName = "L555H" //  Level name
		
		super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		super.viewDidAppear(animated)
		
		doNothing()
		
	}
	
	func turnLeft() {
		for _ in 0..<3 {
			turnRight()
		}
	}
	
	func doubleMove() {
		move()
		
		move()
	}
	
	func doNothing() {
		
	}
}
