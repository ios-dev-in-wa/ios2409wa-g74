//
//  ViewController.swift
//  G74L13
//
//  Created by Ivan Vasilevich on 05.11.2019.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var yellowView: UIImageView!
	@IBOutlet weak var indicator: UIActivityIndicatorView!
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
		indicator.backgroundColor = .yellow
		
		let closure = { [weak self] (image: UIImage) in
			self?.view.backgroundColor = .red
		}
		closure(UIImage())
		
		var unsortedArray = [1, 4, 99, 2]
		unsortedArray.sort { (a, b) -> Bool in
			return a < b
		}
		
		NotificationCenter.default.addObserver(self, selector: #selector(imageDownloaded(notification:)), name: imageDownloadedNotification, object: nil)
	}
	
	@objc func imageDownloaded(notification: Notification) {
		guard let userinfo = notification.userInfo else {
			return
		}
		guard let image = userinfo["pic"] as? UIImage else {
			return
		}
		yellowView.image = image
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
				UIView.animate(withDuration: 0.4) { [unowned self] in
					self.indicator.center = touches.first!.location(in: self.view)
				}
		//		kBgQ.async {
		//			let image = ImageDownloader4000().downloadImageFromLink(link: "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2019/11/04194020/Speedhunters_SEMA_2019_DC06237.jpg")
		//			kMainQueue.async {
		//				self.yellowView.image = image
		//			}
		//		}
		ImageDownloader4000.shared.downloadImageFromLink(link: "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2019/11/04194020/Speedhunters_SEMA_2019_DC06237.jpg")
	}
	
	@IBAction func showAnimation() {
		UIView.transition(with: view, duration: 1, options: [.transitionCurlDown, .autoreverse], animations: {
			self.yellowView.isHidden = !self.yellowView.isHidden
		}) { (complete) in
			print("animation finished")
		}
		
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
		print("deinit of: \(self.theClassName)")
	}
	
}

