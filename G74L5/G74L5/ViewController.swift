//
//  ViewController.swift
//  G74L5
//
//  Created by Ivan Vasilevich on 10/8/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var numberLabel: UILabel!
	@IBOutlet weak var topButton: UIButton!
	var a = 0
	
	
	
	//------------------------------------//
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func awakeFromNib() { //xib // nib
		super.awakeFromNib()
		log()
		print("-------------1-------------------")
		print(topButton?.backgroundColor == nil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		a = 6
		foo()
		square()
		
		view.backgroundColor = UIColor.init(red: 53/255,
											green: 92/255,
											blue: 50/255,
											alpha: 1)
		
		log()
		print("-------------2-------------------")
		print(topButton == nil)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		log()
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		log()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		log()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		log()
		//		tabBarItem.badgeValue = ""
		navigationController?.tabBarItem.badgeValue = nil
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		log()
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		log()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		log()
	}
	//------------------------------------//
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		square()
		updateButtonTitle()
	}
	
	func updateButtonTitle() {
		print(a)
		numberLabel.text = "Result: \(a)"
	}

	func foo() {
		a += 1
	}
	
	@IBAction func square() {
		a = a * a
		updateButtonTitle()
	}
	
	@IBAction func divide() {
		a = a / 100
		updateButtonTitle()
	}
	
}

